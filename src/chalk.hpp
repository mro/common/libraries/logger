/*
** Copyright (C) 2020 Sylvain Fargier
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2020-11-10T19:40:05+01:00
**     Author: Sylvain Fargier <fargie_s> <fargier.sylvain@gmail.com>
**
*/

#ifndef CHALK_HPP__
#define CHALK_HPP__

#include <string>

#include <unistd.h>

namespace logger {

namespace chalk {

struct Color
{
    constexpr Color(const char *value, const char *term, int istty = 0) :
        m_istty(istty), m_begin(value), m_end(term)
    {}
    constexpr Color() : m_istty('n'), m_begin(nullptr), m_end(nullptr) {}
    Color(Color &&) = default;
    Color(const Color &) = default;

    inline bool istty() const
    {
        static int istty = isatty(1);
        return m_istty ? (m_istty == 'y') : istty;
    }

    /**
     * @brief generate a new color for fd
     * @param[in] fd file descriptor associated with color
     * @return new color for this terminal
     */
    Color fd(int fd) const
    {
        return Color(m_begin, m_end, isatty(fd) ? 'y' : 'n');
    }

    inline std::string operator()(const std::string &str) const
    {
        if (istty() && m_begin && m_end)
        {
            std::string result;
            result.reserve(str.size() + 10);

            result += "\033[";
            result += m_begin;
            result += str;
            result += "\033[";
            result += m_end;
            return result;
        }
        else
            return str;
    }

    inline std::string begin() const
    {
        return (istty() && m_begin) ? (std::string("\033[") + m_begin) :
                                      std::string();
    }

    inline std::string end() const
    {
        return (istty() && m_end) ? (std::string("\033[") + m_end) :
                                    std::string();
    }

protected:
    const char m_istty; /* 0=not tested, 'y'=tty, 'n'=not tty */
    const char *m_begin;
    const char *m_end;
};

struct Foreground : Color
{
    constexpr explicit Foreground(const char *value) : Color(value, "39m") {}
};

struct Background : Color
{
    constexpr explicit Background(const char *value) : Color(value, "49m") {}
};

namespace fg {

constexpr Foreground black = Foreground("30m");
constexpr Foreground red = Foreground("31m");
constexpr Foreground green = Foreground("32m");
constexpr Foreground yellow = Foreground("33m");
constexpr Foreground blue = Foreground("34m");
constexpr Foreground magenta = Foreground("35m");
constexpr Foreground cyan = Foreground("36m");
constexpr Foreground white = Foreground("37m");

namespace bright {

constexpr Foreground black = Foreground("90m");
constexpr Foreground red = Foreground("91m");
constexpr Foreground green = Foreground("92m");
constexpr Foreground yellow = Foreground("93m");
constexpr Foreground blue = Foreground("94m");
constexpr Foreground magenta = Foreground("95m");
constexpr Foreground cyan = Foreground("96m");
constexpr Foreground white = Foreground("97m");

} // namespace bright

} // namespace fg

namespace bg {

constexpr Background black = Background("40m");
constexpr Background red = Background("41m");
constexpr Background green = Background("42m");
constexpr Background yellow = Background("43m");
constexpr Background blue = Background("44m");
constexpr Background magenta = Background("45m");
constexpr Background cyan = Background("46m");
constexpr Background white = Background("47m");

namespace bright {

constexpr Background black = Background("100m");
constexpr Background red = Background("101m");
constexpr Background green = Background("102m");
constexpr Background yellow = Background("103m");
constexpr Background blue = Background("104m");
constexpr Background magenta = Background("105m");
constexpr Background cyan = Background("106m");
constexpr Background white = Background("107m");

} // namespace bright

} // namespace bg

constexpr Color bold = Color("1m", "22m");
constexpr Color faint = Color("2m", "22m");
constexpr Color italic = Color("3m", "23m");
constexpr Color underline = Color("4m", "24m");
constexpr Color slowBlink = Color("5m", "25m");
constexpr Color rapidBlink = Color("6m", "25m");
constexpr Color reverse = Color("7m", "27m");
constexpr Color conceal = Color("8m", "28m");
constexpr Color crossedOut = Color("9m", "29m");

} // namespace chalk
} // namespace logger

#endif
