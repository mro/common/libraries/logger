/*
** Copyright (C) 2021 Sylvain Fargier
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2021-10-18T18:44:27+02:00
**     Author: Sylvain Fargier <fargie_s> <fargier.sylvain@free.fr>
**
*/

#ifndef REGEX_HPP__
#define REGEX_HPP__

#include <string>
#include <vector>

#include <regex.h>

namespace logger {

/**
 * @brief posix regex wrapper
 * @details useful with old libstdc++ that have buggy std::regex support
 */
class Regex
{
public:
    /**
     * @param[in] re regex to compile
     * @param[in] flags
     * @throws std::invalid_parameter
     */
    explicit Regex(const std::string &re, int flags = Flags::Extended);
    ~Regex();
    Regex(const Regex &) = delete;
    Regex &operator=(const Regex &) = delete;

    /**
     * @brief execute regex
     * @param[in] str string to run the regex on
     * @param[out] matches matching parts
     * @details vector's capacity will be used to configure the match count,
     * MaxMatchDefault will be used by default
     * @return true if string matches
     */
    bool match(const std::string &str, std::vector<std::string> &matches) const;

    /**
     * @brief execute regex to test for match
     * @param[in] str string to run the regex on
     * @return true if string matches
     */
    bool search(const std::string &str) const;

    enum Flags
    {
        Extended = REG_EXTENDED,
        ICase = REG_ICASE,
        NewLine = REG_NEWLINE
    };

    static constexpr std::size_t MaxMatchDefault = 32;

protected:
    regex_t m_regex;
};

} // namespace logger

#endif
