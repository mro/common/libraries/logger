/*
** Copyright (C) 2021 Sylvain Fargier
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2021-10-18T18:44:27+02:00
**     Author: Sylvain Fargier <fargie_s> <fargier.sylvain@free.fr>
**
*/

#include "Regex.hpp"

#include <stdexcept>

namespace logger {

Regex::Regex(const std::string &re, int flags)
{
    int code = regcomp(&m_regex, re.c_str(), flags);
    if (code != 0)
    {
        size_t sz = regerror(code, &m_regex, nullptr, 0);
        std::vector<char> err(sz + 1);
        regerror(code, &m_regex, err.data(), err.size());
        throw std::invalid_argument(std::string("invalid regex: ") + err.data());
    }
}

Regex::~Regex()
{
    ::regfree(&m_regex);
}

bool Regex::match(const std::string &str,
                  std::vector<std::string> &matches) const
{
    std::vector<regmatch_t> pmatch(matches.capacity() ? matches.capacity() :
                                                        Regex::MaxMatchDefault);
    matches.resize(0);
    if (regexec(&m_regex, str.c_str(), pmatch.size(), pmatch.data(), 0) != 0)
        return false;

    for (const regmatch_t &m : pmatch)
    {
        if (m.rm_so >= 0 && m.rm_eo >= 0)
        {
            matches.push_back(str.substr(m.rm_so, m.rm_eo - m.rm_so));
        }
    }
    return true;
}

bool Regex::search(const std::string &str) const
{
    return regexec(&m_regex, str.c_str(), 0, nullptr, 0) == 0;
}

} // namespace logger
