/*
** Copyright (C) 2020 Sylvain Fargier
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2021-12-30T18:13:05+01:00
**     Author: Sylvain Fargier <fargie_s> <fargier.sylvain@gmail.com>
**
*/

#include "Logger.hpp"

#include <algorithm>
#include <atomic>
#include <cstdlib>
#include <ctime>
#include <iomanip>
#include <iostream>
#include <list>
#include <map>
#include <memory>
#include <mutex>
#include <thread>
#include <tuple>

#include "Regex.hpp"
#include "chalk.hpp"

namespace logger {

struct Filter
{
    std::string name;
    std::unique_ptr<Regex> regex;
    bool enable;
};

struct LogFacility
{
    ~LogFacility();
    void reset();

    bool isEnabled(const std::string &category) const;
    bool isEnabled(Level level) const;
    bool isEnabled(Feature level) const;

    void clearFilters();
    void parseFilter(const std::string &filter);
    void parseFeature(const std::string &feature);
    void addFilter(const std::string &filter, bool value);
    std::string getFilter() const;

    mutable std::mutex mutex;
    std::list<Filter> filters;
    std::atomic<unsigned int> filtersVersion;
    std::atomic<uint8_t> level;
    std::atomic<uint8_t> feature;
    std::atomic<bool> isTTY;
    write_func_t writeFunc;
};

struct ThreadLocalInfo
{
    ThreadLocalInfo();
    ~ThreadLocalInfo();

    const std::string &getThreadName()
    {
        if (!tid)
            return _threadName;
        else if (_threadName.empty())
            _threadName = ("<" + std::to_string(tid) + "> ");
        return _threadName;
    }

    static std::atomic<unsigned int> s_tid;
    const unsigned int tid;

    Logger::Writer writer;
    std::string _threadName;
    std::map<std::string, bool> categories;
    std::size_t maxCategoryLen;
    unsigned int filtersVersion;
    std::ostringstream tmpBuffer;

    std::string lastTimeStr;
    std::time_t lastTimeStamp;
};

template<typename T>
inline constexpr typename std::underlying_type<T>::type enum_cast(T t)
{
    return static_cast<typename std::underlying_type<T>::type>(t);
}

static const std::map<std::string, Level> s_levelNameMap = {
    {"EMERG", Level::EMERG},     {"ALERT", Level::ALERT},
    {"CRIT", Level::CRIT},       {"ERROR", Level::ERROR},
    {"WARNING", Level::WARNING}, {"NOTICE", Level::NOTICE},
    {"INFO", Level::INFO},       {"DEBUG", Level::DEBUG}};
static const std::map<std::string, Feature> s_featureNameMap = {
    {"DATETIME", Feature::DATETIME}, {"TIMEDELTA", Feature::TIMEDELTA},
    {"LEVEL", Feature::LEVEL},       {"CATEGORY", Feature::CATEGORY},
    {"NOCOLOR", Feature::NOCOLOR},   {"THREAD", Feature::THREAD}};

static LogFacility s_facility;

static thread_local ThreadLocalInfo s_threadLocalInfo;

/*
 * when loading libraries, zero initialization is achieved before dynamic init
 * this let us detect whereas library is properly loaded or not.
 *
 * stringstreams do require proper init and can't be zero-init, so null
 * writer will be used until library is properly loaded.
 */
static int s_ready = ([]() {
    s_facility.reset();                // force faciliy creation from main
    s_threadLocalInfo.getThreadName(); // force local_thread creation for main
    return 1;
})();
static std::atomic<unsigned int> s_threadLocalCount{0};

static inline bool isReady()
{
    return s_ready && s_threadLocalCount.load();
}

static const Regex s_starRe("\\*");
static const chalk::Color s_timeColor(chalk::faint.fd(2));
static const chalk::Color s_categoryColor(chalk::fg::blue.fd(2));
static const chalk::Color s_threadColor(chalk::fg::magenta.fd(2));
static const std::string s_empty;

static constexpr Level INVALID_LEVEL = static_cast<Level>(
    enum_cast(Level::DEBUG) + 1);
static constexpr std::size_t MAX_CATEGORY_LEN = 18;

std::atomic<unsigned int> ThreadLocalInfo::s_tid(0);

static inline Logger::Writer &nullWriter()
{
    static Logger::Writer s_nullWriter;
    if (!isReady())
    {
        /* init not ran yet */
        s_nullWriter.buf.setstate(std::ios_base::eofbit);
    }

    return s_nullWriter;
}

static const char *levelIcon(Level level)
{
    switch (level)
    {
    case Level::EMERG: return "\U0001f480";
    case Level::ALERT: return "\U0001f198";
    case Level::CRIT: return "\U0001f4a5";
    case Level::ERROR: return "\U0001f525";
    case Level::WARNING: return "\u2757";
    case Level::NOTICE: return "\U0001f440";
    case Level::INFO: return "\U0001F4AC";
    case Level::DEBUG: return "\U0001f41b";
    default: return "";
    }
}

static const char *levelName(Level level)
{
    switch (level)
    {
    case Level::EMERG: return "[EMERG]  ";
    case Level::ALERT: return "[ALERT]  ";
    case Level::CRIT: return "[CRIT]   ";
    case Level::ERROR: return "[ERROR]  ";
    case Level::WARNING: return "[WARNING]";
    case Level::NOTICE: return "[NOTICE] ";
    case Level::INFO: return "[INFO]   ";
    case Level::DEBUG: return "[DEBUG]  ";
    default: return "";
    }
}

static std::string ms_to_string(const std::chrono::milliseconds &ms)
{
    std::ostringstream &logger(s_threadLocalInfo.tmpBuffer);
    logger.str(s_empty);

    std::chrono::milliseconds::rep count(ms.count());
    if (count <= 99999)
        logger << count << "ms";
    else
    {
        unsigned int reminder;
        if ((reminder = count / (24 * 3600 * 1000)))
        {
            logger << reminder << "d";
            count %= 24 * 3600 * 1000;
        }
        if ((reminder = count / (3600 * 1000)))
        {
            logger << reminder << "h";
            count %= 3600 * 1000;
        }
        if ((reminder = count / (60 * 1000)))
        {
            logger << reminder << "m";
            count %= 60 * 1000;
        }
        if ((reminder = count / (1000)))
            count %= 1000;
        logger << reminder << "." << std::setw(3) << std::setfill('0') << count
               << "s";
    }
    return logger.str();
}

static constexpr std::size_t TIMEDATE_LEN = 20;
static const std::string &lt_to_string(const LocalTime &l)
{
    std::ostringstream &oss(s_threadLocalInfo.tmpBuffer);
    oss.str(s_empty);

    std::chrono::milliseconds::rep now =
        std::chrono::duration_cast<std::chrono::milliseconds>(
            l.time.time_since_epoch())
            .count();
    const std::time_t time = now / 1000;
    if (s_threadLocalInfo.lastTimeStamp == time)
    {
        oss << std::setfill('0') << std::setw(3) << (now % 1000);
        s_threadLocalInfo.lastTimeStr.replace(TIMEDATE_LEN, 3, oss.str());
    }
    else
    {
        struct std::tm tm;

        localtime_r(&time, &tm);
        oss << std::setfill('0');
        oss << (tm.tm_year + 1900) << "-";
        oss << std::setw(2) << (tm.tm_mon + 1) << "-";
        oss << std::setw(2) << tm.tm_mday << " ";
        oss << std::setw(2) << tm.tm_hour << ":";
        oss << std::setw(2) << tm.tm_min << ":";
        oss << std::setw(2) << tm.tm_sec << ".";
        oss << std::setw(3) << (now % 1000);
        s_threadLocalInfo.lastTimeStr = oss.str();
        s_threadLocalInfo.lastTimeStamp = time;
    }
    return s_threadLocalInfo.lastTimeStr;
}

LogFacility::~LogFacility()
{
    s_ready = 0;
}

void LogFacility::reset()
{
    // turn the null writer in a real null writer
    nullWriter().buf.setstate(std::ios_base::eofbit);

    const char *dbg = std::getenv("DEBUG");
    const char *logLevel = std::getenv("LOG_LEVEL");
    const char *logCategory = std::getenv("LOG_CATEGORY");
    const char *logFeature = std::getenv("LOG_FEATURE");

    if (logLevel)
    {
        std::string logLevelStr(logLevel);
        std::transform(logLevelStr.begin(), logLevelStr.end(),
                       logLevelStr.begin(),
                       [](char c) { return std::toupper(c); });
        const auto it = s_levelNameMap.find(logLevelStr);
        if (it != s_levelNameMap.end())
            level.store(static_cast<int>(it->second));
        else
        {
            try
            {
                int l = std::stoi(logLevelStr);
                if (l >= static_cast<int>(Level::EMERG) &&
                    l <= static_cast<int>(Level::DEBUG))
                    level.store(l);
                else
                    throw std::invalid_argument(std::string());
            }
            catch (const std::exception &)
            {
                std::cerr << "Invalid log level: " << logLevel << "\n";
                std::cerr << "Must be one of:";
                for (const auto &v : s_levelNameMap)
                    std::cerr << " " << v.first;
                std::cerr << std::endl;
                level.store(
                    static_cast<int>(dbg ? Level::DEBUG : Level::NOTICE));
            }
        }
    }
    else
        level.store(static_cast<int>(dbg ? Level::DEBUG : Level::NOTICE));

    clearFilters();
    if (logCategory)
        parseFilter(logCategory);
    else if (dbg)
        parseFilter(dbg);

    feature.store(enum_cast(Feature::DATETIME) | enum_cast(Feature::TIMEDELTA) |
                  enum_cast(Feature::LEVEL) | enum_cast(Feature::CATEGORY) |
                  enum_cast(Feature::THREAD));
    if (logFeature)
        parseFeature(logFeature);

    isTTY.store(isatty(2));
    writeFunc = nullptr;
}

bool LogFacility::isEnabled(const std::string &category) const
{
    if (s_threadLocalInfo.filtersVersion != s_facility.filtersVersion)
    {
        s_threadLocalInfo.filtersVersion = s_facility.filtersVersion;
        s_threadLocalInfo.categories.clear();
        s_threadLocalInfo.maxCategoryLen = 0;
    }

    const std::map<std::string, bool>::const_iterator categoryIt =
        s_threadLocalInfo.categories.find(category);
    if (categoryIt == s_threadLocalInfo.categories.end())
    {
        std::unique_lock<std::mutex> lock(mutex);
        auto it = std::find_if(filters.cbegin(), filters.cend(),
                               [&category](const Filter &item) {
                                   return item.regex->search(category);
                               });

        const bool state = (it != filters.cend()) ? it->enable :
                                                    filters.empty();
        lock.unlock();

        if (category.length() > s_threadLocalInfo.maxCategoryLen)
            s_threadLocalInfo.maxCategoryLen = std::min(category.length(),
                                                        MAX_CATEGORY_LEN);
        s_threadLocalInfo.categories.emplace(std::make_pair(category, state));
        return state;
    }
    else
        return categoryIt->second;
}

bool LogFacility::isEnabled(Level l) const
{
    return enum_cast(l) <= level;
}

bool LogFacility::isEnabled(Feature f) const
{
    return feature.load() & enum_cast(f);
}

void LogFacility::clearFilters()
{
    std::lock_guard<std::mutex> lock(mutex);
    filters.clear();
    filtersVersion++;
}

void LogFacility::addFilter(const std::string &category, bool value)
{
    try
    {
        std::lock_guard<std::mutex> lock(mutex);
        filters.emplace_back(Filter{
            category, std::unique_ptr<Regex>(new Regex("^" + category + "$")),
            value});
        filtersVersion++;
    }
    catch (std::exception &ex)
    {
        std::cerr << "Failed to add filter: " << ex.what() << std::endl;
    }
}

void LogFacility::parseFilter(const std::string &filter)
{
    std::size_t start = 0;
    std::size_t end;

    do
    {
        end = filter.find(',', start);

        std::string category = filter.substr(
            start, (end == std::string::npos) ? end : (end - start));
        for (std::size_t i = 0; i < category.length(); ++i)
        {
            if (category[i] == '*')
                category.replace(i++, 1, ".*");
        }

        if (category.empty()) {}
        else if (category[0] == '-')
            addFilter(category.substr(1), false);
        else
            addFilter(category, true);
        start = end + 1;
    } while (end != std::string::npos);
}

std::string LogFacility::getFilter() const
{
    std::lock_guard<std::mutex> lock(mutex);
    std::ostringstream &oss(s_threadLocalInfo.tmpBuffer);
    oss.str(s_empty);

    bool first = true;
    std::for_each(filters.cbegin(), filters.cend(),
                  [&oss, &first](const Filter &f) {
                      if (!first)
                          oss << ",";
                      first = false;

                      if (!f.enable)
                          oss << "-";
                      oss << f.name;
                  });
    return oss.str();
}

void LogFacility::parseFeature(const std::string &featureList)
{
    std::size_t start = 0;
    std::size_t end;

    do
    {
        end = featureList.find(',', start);

        std::string featureStr = featureList.substr(
            start, (end == std::string::npos) ? end : (end - start));
        std::transform(featureStr.begin(), featureStr.end(), featureStr.begin(),
                       [](char c) { return std::toupper(c); });

        bool enable = true;
        if (featureStr.empty()) {}
        else if (featureStr[0] == '-')
        {
            enable = false;
            featureStr = featureStr.substr(1);
        }

        const auto it = s_featureNameMap.find(featureStr);
        if (it != s_featureNameMap.end())
        {
            if (enable)
                feature.store(feature.load() | enum_cast(it->second));
            else
                feature.store(feature.load() & ~enum_cast(it->second));
        }
        else
        {
            std::cerr << "Invalid feature: " << featureStr << "\n";
            std::cerr << "Must be one of:";
            for (const auto &v : s_featureNameMap)
                std::cerr << " " << v.first;
            std::cerr << std::endl;
        }
        start = end + 1;
    } while (end != std::string::npos);
}

ThreadLocalInfo::ThreadLocalInfo() :
    tid(s_tid++), maxCategoryLen(0), filtersVersion(0), lastTimeStamp(0)
{
    s_threadLocalCount += 1;
}

ThreadLocalInfo::~ThreadLocalInfo()
{
    s_threadLocalCount -= 1;
}

Level Config::getLevel() const
{
    return static_cast<Level>(s_facility.level.load());
}

Config &Config::reset()
{
    s_facility.reset();
    return *this;
}

Config &Config::setLevel(Level level)
{
    s_facility.level.store(static_cast<int>(level));
    return *this;
}

Config &Config::setFilter(const std::string &filter)
{
    s_facility.clearFilters();
    addFilter(filter);
    return *this;
}

Config &Config::addFilter(const std::string &filter)
{
    s_facility.parseFilter(filter);
    return *this;
}

std::string Config::getFilter() const
{
    return s_facility.getFilter();
}

Config &Config::enable(Feature feature)
{
    s_facility.feature |= enum_cast(feature);
    return *this;
}

Config &Config::disable(Feature feature)
{
    s_facility.feature &= ~enum_cast(feature);
    return *this;
}

bool Config::isEnabled(Feature feature) const
{
    return s_facility.isEnabled(feature);
}

Config &Config::setWriteFunc(write_func_t fun)
{
    s_facility.writeFunc = fun;
    return *this;
}

Logger::Logger(Level level, Writer &writer) : writer(writer), level(level)
{
    writer.prepare(level);
}

Logger::Logger(Logger &&other) : writer(other.writer), level(other.level)
{
    const_cast<Level &>(other.level) = INVALID_LEVEL;
}

Logger::~Logger()
{
    if (&writer != &nullWriter() && level != INVALID_LEVEL)
        writer.flush(level);
}

Logger::Writer &Logger::defaultWriter()
{
    if (!isReady()) // unlikely
        return nullWriter();
    return s_threadLocalInfo.writer;
}

void Logger::Writer::prepare(Level level)
{
    if (!isReady() || !s_facility.isEnabled(level))
        return;

    const uint8_t features = s_facility.feature;
    if (features & enum_cast(Feature::NOCOLOR))
    {
        if (features & enum_cast(Feature::DATETIME))
            buf << lt_to_string(LocalTime()) << " ";

        if (features & enum_cast(Feature::TIMEDELTA))
        {
            std::string value = ms_to_string(last.tick());
            buf << "(" << std::setfill(' ') << std::setw(7) << value << ") ";
        }

        if (features & enum_cast(Feature::LEVEL))
            buf << levelName(level) << " ";

        // s_tid contains next thread id, thus main gets 0 and increments
        if (features & enum_cast(Feature::THREAD) && ThreadLocalInfo::s_tid > 1)
            buf << s_threadLocalInfo.getThreadName();
    }
    else
    {
        if (features & enum_cast(Feature::DATETIME))
            buf << s_timeColor(lt_to_string(LocalTime())) << " ";

        if (features & enum_cast(Feature::TIMEDELTA))
        {
            std::string value = ms_to_string(last.tick());
            buf << s_timeColor.begin() << "(" << std::setfill(' ')
                << std::setw(7) << value << ")" << s_timeColor.end() << " ";
        }

        if (features & enum_cast(Feature::LEVEL))
            buf << (s_facility.isTTY ? levelIcon(level) : levelName(level))
                << " ";

        // s_tid contains next thread id, thus main gets 0 and increments
        if (features & enum_cast(Feature::THREAD) && ThreadLocalInfo::s_tid > 1)
            buf << s_threadColor.begin() << s_threadLocalInfo.getThreadName()
                << s_threadColor.end();
    }
    prefixEnd = buf.tellp();
}

void Logger::Writer::flush(Level level)
{
    if (buf.tellp() <= prefixEnd) {}
    else if (s_facility.isEnabled(level))
    {
        buf.put('\n');
        const std::string line(buf.str());
        if (!s_facility.writeFunc || !s_facility.writeFunc(level, line))
        {
            // not calling std::endl to dump everything at once
            // no mutex needed since line is dumped in a single write
            std::cerr << line << std::flush;
        }
    }

    if (!buf.eof())
    {
        buf.str(s_empty);
    }
}

std::string Logger::Writer::str()
{
    return std::move(buf.str());
}

LocalTime::LocalTime(const time_point &time) : time(time) {}
LocalTime::LocalTime() : time(std::chrono::system_clock::now()) {}

std::string to_string(const LocalTime &l)
{
    return lt_to_string(l);
}

Elapsed::Elapsed() : start(std::chrono::steady_clock::now()) {}

std::chrono::milliseconds Elapsed::tick()
{
    decltype(start) now = std::chrono::steady_clock::now();
    std::chrono::milliseconds ms =
        std::chrono::duration_cast<std::chrono::milliseconds>(now - start);
    start = now;
    return ms;
}

const Logger &operator<<(const Logger &logger,
                         const std::chrono::milliseconds &ms)
{
    return (logger << ms_to_string(ms));
}

NullLogger::NullLogger(Level level) : Logger(level, nullWriter()) {}

Logger log(Level level, const std::string &category)
{
    if (!isReady()) // unlikely ()
    {
        static Logger::Writer s_writer;
        s_writer.buf.setstate(std::ios_base::eofbit);
        return Logger(level, s_writer);
    }
    const bool enabled = s_facility.isEnabled(level) &&
        s_facility.isEnabled(category);

    if (enabled && s_facility.isEnabled(Feature::CATEGORY))
    {
        const bool isColor = !s_facility.isEnabled(Feature::NOCOLOR);
        // is zero if no category ever used
        const std::size_t maxLen = s_threadLocalInfo.maxCategoryLen;
        if (maxLen > 0)
        {
            const std::size_t fill = (maxLen > category.length()) ?
                (maxLen - category.length() + 1) :
                1;
            return std::move(const_cast<Logger &>(
                Logger(level, enabled ? s_threadLocalInfo.writer : nullWriter())
                << (isColor ? s_categoryColor.begin() : std::string())
                << (category.empty() ? std::string("  ") :
                                       ("{" + category + "}"))
                << (isColor ? s_categoryColor.end() : std::string())
                << std::string(fill, ' ')));
        }
    }
    return Logger(level, enabled ? s_threadLocalInfo.writer : nullWriter());
}

} // namespace logger