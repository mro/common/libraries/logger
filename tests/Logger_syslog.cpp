/*
** Copyright (C) 2022 Sylvain Fargier
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2022-10-18T10:21:22
** Author: Sylvain Fargier <fargier.sylvain@gmail.com>
*/

#include <syslog.h>

#include "Logger.hpp"

int main()
{
    openlog("myapp", LOG_PID, LOG_USER);

    logger::Config()
        .setLevel(logger::Level::DEBUG)
        .setFilter("")
        .disable(logger::Feature::LEVEL)
        .disable(logger::Feature::DATETIME)
        .disable(logger::Feature::TIMEDELTA)
        .enable(logger::Feature::CATEGORY)
        .enable(logger::Feature::NOCOLOR)
        .setWriteFunc([](logger::Level level, const std::string &line) {
            switch (level)
            {
            case logger::Level::EMERG:
                syslog(LOG_EMERG, "%s", line.c_str());
                break;
            case logger::Level::ALERT:
                syslog(LOG_ALERT, "%s", line.c_str());
                break;
            case logger::Level::CRIT:
                syslog(LOG_CRIT, "%s", line.c_str());
                break;
            case logger::Level::ERROR:
                syslog(LOG_ERR, "%s", line.c_str());
                break;
            case logger::Level::WARNING:
                syslog(LOG_WARNING, "%s", line.c_str());
                break;
            case logger::Level::NOTICE:
                syslog(LOG_NOTICE, "%s", line.c_str());
                break;
            case logger::Level::INFO:
                syslog(LOG_INFO, "%s", line.c_str());
                break;
            case logger::Level::DEBUG:
                syslog(LOG_DEBUG, "%s", line.c_str());
                break;
            default: break;
            }
            return true;
        });
    ;

    logger::debug() << "debug message";
    logger::info() << "info message";
    logger::notice() << "notice message";
    logger::warning() << "warning message";
    logger::error() << "error message";
    logger::crit("category") << "critical message";
    logger::emerg() << "emergency message";

    logger::Config().setWriteFunc(nullptr);
    closelog();
}