/*
** Copyright (C) 2022 Sylvain Fargier
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2022-10-19T21:49:33
** Author: Sylvain Fargier <fargier.sylvain@gmail.com>
*/

#include <cstdlib>
#include <list>
#include <map>
#include <set>
#include <tuple>
#include <vector>

#include <cppunit/TestFixture.h>

#include "Logger.hpp"
#include "test_helpers.hpp"

using namespace logger;

class TestLoggerConfig : public CppUnit::TestFixture
{
protected:
    CPPUNIT_TEST_SUITE(TestLoggerConfig);
    CPPUNIT_TEST(features);
    CPPUNIT_TEST_SUITE_END();

    Level m_level;

public:
    void setUp()
    {
        m_level = Config().getLevel();
        Config()
            .reset()
            .setLevel(Level::DEBUG)
            .setFilter("")
            .disable(Feature::LEVEL)
            .disable(Feature::DATETIME)
            .disable(Feature::TIMEDELTA)
            .disable(Feature::CATEGORY)
            .enable(Feature::NOCOLOR);
        std::cerr << std::endl;
    }

    void tearDown()
    {
        Config().setLevel(m_level);
        ::unsetenv("LOG_FEATURE");
    }

    void features()
    {
        ::setenv("LOG_FEATURE",
                 "LEVEL,-DATETIME,XXX,TIMEDELTA,-CATEGORY,NOCOLOR,-THREAD", 1);
        Config().reset();
        std::vector<Feature> featureList{Feature::LEVEL,     Feature::DATETIME,
                                         Feature::TIMEDELTA, Feature::CATEGORY,
                                         Feature::NOCOLOR,   Feature::CATEGORY};
        for (size_t i = 0; i < featureList.size(); ++i)
            EQ(!(i & 1), Config().isEnabled(featureList[i]));

        ::setenv("LOG_FEATURE",
                 "-LEVEL,DATETIME,-TIMEDELTA,CATEGORY,-NOCOLOR,THREAD", 1);
        Config().reset();
        for (size_t i = 0; i < featureList.size(); ++i)
            EQ(!!(i & 1), Config().isEnabled(featureList[i]));
    }
};

CPPUNIT_TEST_SUITE_REGISTRATION(TestLoggerConfig);
