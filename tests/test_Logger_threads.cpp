/*
** Copyright (C) 2021 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2022-01-10T16:50:05+01:00
**     Author: Sylvain Fargier <sfargier> <sylvain.fargier@cern.ch>
**
*/

#include <iostream>
#include <numeric>
#include <thread>
#include <vector>

#include <cppunit/TestFixture.h>

#include "Logger.hpp"
#include "Logger_Examples.hpp"
#include "test_helpers.hpp"

using namespace logger;

class TestLoggerThreads : public CppUnit::TestFixture
{
protected:
    CPPUNIT_TEST_SUITE(TestLoggerThreads);
    CPPUNIT_TEST(filters);
    CPPUNIT_TEST(threadId);
    CPPUNIT_TEST_SUITE_END();

    Level m_level;

public:
    void setUp()
    {
        m_level = Config().getLevel();
        Config()
            .reset()
            .setLevel(Level::DEBUG)
            .setFilter("")
            .disable(Feature::LEVEL)
            .disable(Feature::DATETIME)
            .disable(Feature::TIMEDELTA)
            .disable(Feature::CATEGORY)
            .enable(Feature::NOCOLOR);
        std::cerr << std::endl;
    }

    void tearDown() { Config().setLevel(m_level); }

    void filters()
    {
        /* ensure filter modifications are take in account by all threads */
        std::condition_variable cv;
        std::mutex m;

        Config().disable(Feature::THREAD);
        Config().setFilter("-example,*");
        std::unique_lock<std::mutex> lock(m);

        auto t = std::async(std::launch::async, [&cv, &m]() {
            std::unique_lock<std::mutex> lock(m);
            cv.notify_one();

            {
                /* filtered */
                Logger l(info("example"));
                l << "text";
                EQ(std::string(), l.writer.str());
            }

            cv.wait(lock);
            {
                /* not filtered */
                Logger l(info("example"));
                l << "text";
                EQ(std::string("text"), l.writer.str());
            }
        });
        cv.wait_for(lock, std::chrono::milliseconds(200));

        Config().setFilter(std::string());
        cv.notify_one();
        lock.unlock();
        t.get();
    }

    void threadId()
    {
        Config().enable(Feature::THREAD);
        std::string ret;
        unsigned int tid;

        ret = (notice() << "test").writer.str();
        EQ(std::string("test"), ret);

        std::thread([&tid]() {
            std::string ret = (notice() << "test").writer.str();
            EQ(false, ret.empty());
            EQ('<', ret[0]);

            tid = std::stoi(ret.substr(1));
        }).join();
        std::thread([&tid]() {
            std::string ret = (notice() << "test").writer.str();
            EQ("<" + std::to_string(tid + 1) + "> test", ret);
        }).join();
    }
};

CPPUNIT_TEST_SUITE_REGISTRATION(TestLoggerThreads);
