/*
** Copyright (C) 2020 Sylvain Fargier
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2022-01-09T12:08:04+01:00
**     Author: Sylvain Fargier <fargie_s> <fargier.sylvain@gmail.com>
**
*/

#include <algorithm>
#include <chrono>
#include <future>
#include <iostream>
#include <thread>
#include <vector>

#include "Logger.hpp"
#include "Logger_Examples.hpp"

using namespace logger;
using std::chrono::milliseconds;
using std::chrono::steady_clock;

int main()
{
    notice() << "this is a message";

    notice("app") << "a message in \"app\" category";
    std::this_thread::sleep_for(milliseconds(200));
    notice("app") << "200 ms later";

    {
        Logger l(notice("app:overrides"));
        l << "sub-category dumping objects: " << std::endl;
        l << "\t"
          << "object with toString() method: " << HasToString() << std::endl;
        l << "\t"
          << "object with external to_string() function: "
          << HasExternalToString() << std::endl;
        l << "\t"
          << "object with direct Logger operator<< override: "
          << HasDirectOverride();
    }

    debug("app:levels") << "debug message";
    info("app:levels") << "info message";
    notice("app:levels") << "notice message";
    warning("app:levels") << "warning message";
    error("app:levels") << "error message";
    crit("app:levels") << "critical message";
    emerg("app:levels") << "emergency message";

    std::vector<std::future<void>> workers;
    workers.reserve(10);
    steady_clock::time_point deadline = steady_clock::now() + milliseconds(20);
    for (std::size_t num = 0; num < workers.capacity(); ++num)
    {
        workers.push_back(std::async(std::launch::async, [num, &deadline]() {
            std::this_thread::sleep_until(deadline);
            info("app:worker") << "message from worker(" << num << ")";
            debug("app:worker") << "some debug info";
        }));
    }
    std::for_each(workers.begin(), workers.end(),
                  [](std::future<void> &w) { w.wait(); });
    return 0;
}
