/*
** Copyright (C) 2020 Sylvain Fargier
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2022-01-04T09:34:22+01:00
**     Author: Sylvain Fargier <fargie_s> <fargier.sylvain@gmail.com>
**
*/

#ifndef LOGGER_EXAMPLES_HPP__
#define LOGGER_EXAMPLES_HPP__

#include <string>

#include "Logger.hpp"

/**
 * @brief can be logged using "toString" method
 */
class HasToString
{
public:
    inline std::string toString() const { return "HasToString"; }
};

/**
 * @brief can be logged using external "to_string" function
 */
class HasExternalToString
{};

inline std::string to_string(const HasExternalToString &)
{
    return "HasExternalToString";
}

/**
 * @brief prefer external "to_string" function
 */
class HasBothToString
{
public:
    inline std::string toString() const { return "HasToString"; }
};

inline std::string to_string(const HasBothToString &)
{
    return "HasExternalToString";
}

/**
 * @brief can be logged using inheritance
 */
class InheritToString : public HasBothToString
{};

/**
 * @brief prefer logger dedicated operator
 */
class HasDirectOverride : public HasBothToString
{};

inline const logger::Logger &operator<<(const logger::Logger &logger,
                                        const HasDirectOverride &)
{
    return (logger << "HasDirectOverride");
}

#endif
