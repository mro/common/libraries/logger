/*
** Copyright (C) 2022 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2022-07-19T11:20:32
** Author: Sylvain Fargier <sylvain.fargier@cern.ch>
*/

#include <type_traits>

#include <cppunit/TestFixture.h>

#include "Logger.hpp"
#include "Logger_Examples.hpp"
#include "test_helpers.hpp"

using namespace logger;
namespace chrono = std::chrono;

struct Test
{
    int i{12};
};

namespace logger {
template<typename T>
struct exclude_default<T, typename std::is_same<Test, T>::type> : std::true_type
{};

/* work-arround gcc-4.8.5 bug where can_ostringstream always return true */
template<typename T>
typename std::enable_if<std::is_same<Test, T>::value, const Logger &>::type operator<<(
    const Logger &logger,
    const T &value)
{
    if (logger.isEnabled())
        logger << value.i;
    return logger;
}

template<typename T>
struct exclude_default<T, typename std::is_same<std::decay<float>::type, T>::type> :
    std::true_type
{};

/* override float prints */
template<typename T>
typename std::enable_if<std::is_same<std::decay<float>::type, T>::value,
                        const Logger &>::type
    operator<<(const Logger &logger, const T &)
{
    if (logger.isEnabled())
        logger << "float_str";
    return logger;
}

} // namespace logger

class TestLoggerCustom : public CppUnit::TestFixture
{
protected:
    CPPUNIT_TEST_SUITE(TestLoggerCustom);
    CPPUNIT_TEST(simple);
    CPPUNIT_TEST_SUITE_END();

    Level m_level;

public:
    void setUp()
    {
        m_level = Config().getLevel();
        Config()
            .reset()
            .setLevel(Level::DEBUG)
            .setFilter("")
            .disable(Feature::LEVEL)
            .disable(Feature::DATETIME)
            .disable(Feature::TIMEDELTA)
            .disable(Feature::CATEGORY)
            .enable(Feature::NOCOLOR);
        std::cerr << std::endl;
    }

    void tearDown() { Config().setLevel(m_level); }

    void simple()
    {
        debug() << Test{};

        {
            Logger l(info("toto"));
            l << float(42);
            EQ(std::string("float_str"), l.writer.str());
        }
    }
};

CPPUNIT_TEST_SUITE_REGISTRATION(TestLoggerCustom);
