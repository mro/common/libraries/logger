/*
** Copyright (C) 2023 Sylvain Fargier
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2023-12-19T15:07:31
** Author: Sylvain Fargier <fargier.sylvain@gmail.com>
*/

#include <cppunit/TestFixture.h>

#include "Logger.hpp"
#include "test_helpers.hpp"

using namespace logger;

class TestLoggerFmt : public CppUnit::TestFixture
{
protected:
    CPPUNIT_TEST_SUITE(TestLoggerFmt);
    CPPUNIT_TEST(hex);
    CPPUNIT_TEST_SUITE_END();

    Level m_level;

public:
    void setUp()
    {
        m_level = Config().getLevel();
        Config()
            .reset()
            .setLevel(Level::DEBUG)
            .setFilter("")
            .disable(Feature::LEVEL)
            .disable(Feature::DATETIME)
            .disable(Feature::TIMEDELTA)
            .disable(Feature::CATEGORY)
            .enable(Feature::NOCOLOR);
        std::cerr << std::endl;
    }

    void tearDown() { Config().setLevel(m_level); }

    void hex()
    {
        {
            Logger l(Level::NOTICE);
            l << logger::hex(uint8_t(0x12)) << " "
              << logger::hex(uint16_t(0x1234)) << " "
              << logger::hex(uint32_t(0x12345678)) << " "
              << logger::hex(uint64_t(0x1234567812345678));

            EQ(std::string("12 1234 12345678 1234567812345678"), l.writer.str());
        }

        { /* signed values */
            Logger l(Level::NOTICE);
            l << logger::hex(int8_t(0x12)) << " " << logger::hex(int8_t(-0x12))
              << " " << logger::hex(int16_t(-0x1234)) << " "
              << logger::hex(int32_t(-0x12345678)) << " "
              << logger::hex(int64_t(0x1234567812345678)) << " "
              << logger::hex(int64_t(-0x1234567812345678));

            EQ(std::string(
                   "12 EE EDCC EDCBA988 1234567812345678 EDCBA987EDCBA988"),
               l.writer.str());
        }

        { /* with prefix */
            Logger l(Level::NOTICE);
            l << logger::hex(uint8_t(0x12), true) << " "
              << logger::hex(int16_t(0x1234), true) << " "
              << logger::hex(uint32_t(0x12345678), true) << " "
              << logger::hex(int64_t(0x1234567812345678), true);
            EQ(std::string("0x12 0x1234 0x12345678 0x1234567812345678"),
               l.writer.str());
        }
    }
};

CPPUNIT_TEST_SUITE_REGISTRATION(TestLoggerFmt);
