/*
** Copyright (C) 2020 Sylvain Fargier
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2021-12-30T15:25:06+01:00
**     Author: Sylvain Fargier <fargie_s> <fargier.sylvain@gmail.com>
**
*/

#include <iostream>

#include <cppunit/TestFixture.h>

#include "chalk.hpp"
#include "test_helpers.hpp"

using namespace logger;

class TestChalk : public CppUnit::TestFixture
{
protected:
    CPPUNIT_TEST_SUITE(TestChalk);
    CPPUNIT_TEST(simple);
    CPPUNIT_TEST_SUITE_END();

public:
    void simple()
    {
        const std::vector<std::string> data = {
            chalk::fg::black("black"),
            chalk::fg::red("red"),
            chalk::fg::green("green"),
            chalk::fg::yellow("yellow"),
            chalk::fg::blue("blue"),
            chalk::fg::magenta("magenta"),
            chalk::fg::cyan("cyan"),
            chalk::fg::white("white"),

            chalk::bg::black("black"),
            chalk::bg::red("red"),
            chalk::bg::green("green"),
            chalk::bg::yellow("yellow"),
            chalk::bg::blue("blue"),
            chalk::bg::magenta("magenta"),
            chalk::bg::cyan("cyan"),
            chalk::bg::white("white"),

            chalk::bg::magenta(chalk::fg::blue("magenta bg blue fg")),
            chalk::fg::bright::red("bright red"),
            chalk::fg::bright::yellow("bright yellow"),

            chalk::bold("bold"),
            chalk::faint("faint"),
            chalk::italic("italic"),
            chalk::underline("underline"),
            chalk::slowBlink("slow blink"),
            chalk::rapidBlink("rapid blink"),
            chalk::reverse("reverse"),
            chalk::conceal("conceal"),
            chalk::crossedOut("crossed-out")};
        for (const std::string &str : data)
        {
            std::cout << str << std::endl;
        }
    }
};

CPPUNIT_TEST_SUITE_REGISTRATION(TestChalk);
