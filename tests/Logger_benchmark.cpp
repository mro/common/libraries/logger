/*
** Copyright (C) 2020 Sylvain Fargier
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2021-12-31T11:41:31+01:00
**     Author: Sylvain Fargier <fargie_s> <fargier.sylvain@gmail.com>
**
*/

#include <iostream>
#include <numeric>
#include <thread>
#include <vector>

#include <cppunit/TestFixture.h>

#include "Logger.hpp"
#include "Logger_Examples.hpp"
#include "test_helpers.hpp"

using namespace logger;
namespace chrono = std::chrono;

class TestLoggerBenchmark : public CppUnit::TestFixture
{
protected:
    CPPUNIT_TEST_SUITE(TestLoggerBenchmark);
    CPPUNIT_TEST(threaded);
    CPPUNIT_TEST(mono);
    CPPUNIT_TEST_SUITE_END();

    Level m_level;

public:
    void setUp()
    {
        m_level = Config().getLevel();
        Config()
            .reset()
            .setLevel(Level::DEBUG)
            .setFilter("")
            .enable(Feature::DATETIME)
            .enable(Feature::TIMEDELTA);
        std::cerr << std::endl;
    }

    void tearDown()
    {
        Config().setLevel(m_level);
        std::cerr << "For better results redirect your stderr to /dev/null"
                  << std::endl;
    }

    void threaded()
    {
        const std::size_t count = 100;
        const chrono::seconds duration(5);

        std::vector<std::thread> threads;
        threads.reserve(count);
        std::vector<std::size_t> counter(count, 0);

        chrono::steady_clock::time_point deadline = chrono::steady_clock::now() +
            duration;
        for (std::size_t i = 0; i < count; ++i)
        {
            threads.emplace_back([i, &counter, &deadline]() {
                while (chrono::steady_clock::now() < deadline)
                {
                    notice() << "message from thread " << i;
                    counter[i]++;
                }
            });
        }
        for (std::size_t i = 0; i < count; ++i)
            threads[i].join();
        std::size_t total = std::accumulate(counter.begin(), counter.end(), 0);
        std::cout << "\n\t" << total << " messages in " << duration.count()
                  << " seconds: " << (total / duration.count()) << " m/s"
                  << std::endl;
    }

    void mono()
    {
        const std::size_t count = 100000;
        chrono::steady_clock::time_point start = chrono::steady_clock::now();
        for (std::size_t i = 0; i < count; ++i)
        {
            notice("test") << "this is a test: " << 42;
        }
        chrono::steady_clock::time_point end = chrono::steady_clock::now();
        std::chrono::nanoseconds duration =
            std::chrono::duration_cast<std::chrono::nanoseconds>(end - start);
        std::cout << "\n\t" << count << " messages in " << duration.count()
                  << " nanoseconds: " << (count * 1000000000 / duration.count())
                  << " m/s" << std::endl;
    }
};

CPPUNIT_TEST_SUITE_REGISTRATION(TestLoggerBenchmark);
