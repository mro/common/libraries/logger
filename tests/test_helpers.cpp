/*
** Copyright (C) 2018 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2018-10-09T13:29:47+02:00
**     Author: Sylvain Fargier <fargie_s> <sylvain.fargier@cern.ch>
**
*/

#include "test_helpers.hpp"

#include <iomanip>
#include <sstream>

CPPUNIT_NS_BEGIN

    CPPUNIT_TRAITS_OVERRIDE(std::future_status)
    {
        std::ostringstream oss;

        oss << "std::future_status(";
        switch (t)
        {
        case std::future_status::ready: oss << "ready)"; break;
        case std::future_status::timeout: oss << "timeout)"; break;
        case std::future_status::deferred: oss << "deferred)"; break;
        };
        return oss.str();
    }

    CPPUNIT_TRAITS_OVERRIDE(std::vector<uint8_t>)
    {
        std::ostringstream oss;

        oss << "Buffer{";
        for (uint8_t v : t)
        {
            oss << " " << std::hex << std::setfill('0') << std::setw(2)
                << int(v);
        }
        oss << " }";
        return oss.str();
    }

    CPPUNIT_TRAITS_OVERRIDE(std::string)
    {
        std::ostringstream oss;

        oss << "string(\"" << t << "\")";
        return oss.str();
    }

    CPPUNIT_TRAITS_OVERRIDE(std::set<std::string>)
    {
        std::ostringstream oss;

        oss << "Set{";
        for (const std::string &v : t)
        {
            oss << " \"" << v << "\"";
        }
        oss << " }";
        return oss.str();
    }

CPPUNIT_NS_END

void printException(const CPPUNIT_NS::Exception &ex)
{
    CPPUNIT_NS::SourceLine line = ex.sourceLine();
    std::cerr << "Failure: ";
    if (line.isValid())
        std::cerr << "Failure: " << line.fileName() << ":" << line.lineNumber()
                  << std::endl;
    else
        std::cerr << "Failure:" << std::endl;

    std::cerr << ex.message().shortDescription() << "\n"
              << ex.message().details() << std::endl;
}

void assertContains(const std::string &haystack,
                    const std::string &needle,
                    const CPPUNIT_NS::SourceLine &line)
{
    CPPUNIT_NS::Asserter::failIf(
        haystack.find(needle) == std::string::npos,
        CPPUNIT_NS::Message("assertion failed",
                            "Expected  : \"" + haystack + "\"",
                            "To contain: \"" + needle + "\""),
        line);
}
