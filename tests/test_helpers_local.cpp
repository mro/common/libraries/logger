/*
** Copyright (C) 2022 Sylvain Fargier
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2022-10-18T10:42:15
** Author: Sylvain Fargier <fargier.sylvain@gmail.com>
*/

#include "test_helpers_local.hpp"

CPPUNIT_NS_BEGIN

    CPPUNIT_TRAITS_OVERRIDE(logger::Level)
    {
        switch (t)
        {
        case logger::Level::EMERG: return "EMERG";
        case logger::Level::CRIT: return "CRIT";
        case logger::Level::WARNING: return "WARNING";
        case logger::Level::INFO: return "INFO";
        case logger::Level::ALERT: return "ALERT";
        case logger::Level::ERROR: return "ERROR";
        case logger::Level::NOTICE: return "NOTICE";
        case logger::Level::DEBUG: return "DEBUG";
        default:
            return "UNKNOWN(" +
                std::to_string(std::underlying_type<logger::Level>::type(t)) +
                ")";
        }
    }

CPPUNIT_NS_END