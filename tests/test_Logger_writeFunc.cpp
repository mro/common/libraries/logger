/*
** Copyright (C) 2020 Sylvain Fargier
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2022-10-18T10:21:42
**     Author: Sylvain Fargier <fargie_s> <fargier.sylvain@gmail.com>
**
*/

#include <list>
#include <map>
#include <set>
#include <tuple>
#include <vector>

#include <cppunit/TestFixture.h>

#include "config.h"

#ifdef HAVE_SYSLOG
#include <syslog.h>
#endif

#include "Logger.hpp"
#include "test_helpers_local.hpp"

using namespace logger;

class TestLoggerWriteFunc : public CppUnit::TestFixture
{
protected:
    CPPUNIT_TEST_SUITE(TestLoggerWriteFunc);
    CPPUNIT_TEST(simple);
#ifdef HAVE_SYSLOG
    CPPUNIT_TEST(syslogWriteFunc);
#endif
    CPPUNIT_TEST_SUITE_END();

    Level m_level;

public:
    void setUp()
    {
        m_level = Config().getLevel();
        Config()
            .reset()
            .setLevel(Level::DEBUG)
            .setFilter("")
            .disable(Feature::LEVEL)
            .disable(Feature::DATETIME)
            .disable(Feature::TIMEDELTA)
            .disable(Feature::CATEGORY)
            .enable(Feature::NOCOLOR);
        std::cerr << std::endl;
    }

    void tearDown() { Config().setLevel(m_level).setWriteFunc(nullptr); }

    void simple()
    {
        std::vector<std::pair<Level, std::string>> lines;
        Config()
            .setWriteFunc([&lines](Level level, const std::string &line) {
                lines.push_back(std::make_pair(level, line));
                return true;
            })
            .setLevel(Level::NOTICE);

        error("test") << "This is a test";
        info() << "filtered";
        warning() << 12 << " and " << 13;
        debug() << "filtered";

        notice(); // empty messages are ignored

        Config()
            .enable(Feature::CATEGORY)
            .enable(Feature::LEVEL)
            .enable(Feature::NOCOLOR);
        notice("cat") << "with category and level";

        std::vector<std::pair<Level, std::string>> expect{
            {Level::ERROR, "This is a test\n"},
            {Level::WARNING, "12 and 13\n"},
            {Level::NOTICE, "[NOTICE]  {cat}  with category and level\n"}};
        EQ(expect.size(), lines.size());
        for (size_t i = 0; i < expect.size(); ++i)
        {
            EQ(expect[i].first, lines[i].first);
            EQ(expect[i].second, lines[i].second);
        }
    }

#ifdef HAVE_SYSLOG
    void syslogWriteFunc()
    {
        // example syslog integration
        Config().setWriteFunc([](Level level, const std::string &line) {
            switch (level)
            {
            case Level::EMERG: syslog(LOG_EMERG, "%s", line.c_str()); break;
            case Level::ALERT: syslog(LOG_ALERT, "%s", line.c_str()); break;
            case Level::CRIT: syslog(LOG_CRIT, "%s", line.c_str()); break;
            case Level::ERROR: syslog(LOG_ERR, "%s", line.c_str()); break;
            case Level::WARNING: syslog(LOG_WARNING, "%s", line.c_str()); break;
            case Level::NOTICE: syslog(LOG_NOTICE, "%s", line.c_str()); break;
            case Level::INFO: syslog(LOG_INFO, "%s", line.c_str()); break;
            case Level::DEBUG: syslog(LOG_DEBUG, "%s", line.c_str()); break;
            default: break;
            }
            return true;
        });

        debug() << "debug message";
        info() << "info message";
        notice() << "notice message";
        warning() << "warning message";
        error() << "error message";
        crit() << "critical message";
        emerg() << "emergency message";
    }
#endif
};

CPPUNIT_TEST_SUITE_REGISTRATION(TestLoggerWriteFunc);
