/*
** Copyright (C) 2020 Sylvain Fargier
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2021-12-31T11:41:31+01:00
**     Author: Sylvain Fargier <fargie_s> <fargier.sylvain@gmail.com>
**
*/

#include <iomanip>
#include <iostream>
#include <numeric>
#include <thread>
#include <vector>

#include <cppunit/TestFixture.h>

#include "Logger.hpp"
#include "Logger_Examples.hpp"
#include "test_helpers.hpp"

using namespace logger;
namespace chrono = std::chrono;

class TestLogger : public CppUnit::TestFixture
{
protected:
    CPPUNIT_TEST_SUITE(TestLogger);
    CPPUNIT_TEST(simple);
    CPPUNIT_TEST(nullLogger);
    CPPUNIT_TEST(disabledLogger);
    CPPUNIT_TEST(filters);
    CPPUNIT_TEST(toStringObjects);
    CPPUNIT_TEST(elapsed);
    CPPUNIT_TEST(localTime);
    CPPUNIT_TEST(category);
    CPPUNIT_TEST(ios);
    CPPUNIT_TEST(ndebug);
    CPPUNIT_TEST_SUITE_END();

    Level m_level;

public:
    void setUp()
    {
        m_level = Config().getLevel();
        Config()
            .reset()
            .setLevel(Level::DEBUG)
            .setFilter("")
            .disable(Feature::LEVEL)
            .disable(Feature::DATETIME)
            .disable(Feature::TIMEDELTA)
            .disable(Feature::CATEGORY)
            .enable(Feature::NOCOLOR);
        std::cerr << std::endl;
    }

    void tearDown() { Config().setLevel(m_level); }

    void simple()
    {
        Config().reset().setFilter("").setLevel(Level::DEBUG);
        debug() << "debug message";
        std::this_thread::sleep_for(std::chrono::milliseconds(200));
        debug() << "debug message";
        info() << "info message";
        notice() << "notice message";
        warning() << "warning message";
        error() << "error message";
        crit() << "critical message";
        emerg() << "emergency message";

        debug("app") << "debug message in app category";
        debug("app:part") << "debug message in app:part category";
        debug("app:another") << "debug message in app:another category";
        debug("app:part") << "debug message in app:part category";
        debug("app") << "debug message in app category";

        debug() << "no category message";
    }

    void nullLogger()
    {
        NullLogger nullLogger(Level::CRIT);
        nullLogger << "test";
        EQ(true, nullLogger.writer.buf.eof());
        EQ(std::string(), nullLogger.writer.str());
    }

    void disabledLogger()
    {
        {
            Config().setLevel(Level::NOTICE);
            Logger l = info();
            EQ(l.writer.buf.eof(), true);
            l << "test";
            EQ(l.writer.buf.eof(), true);
            EQ(std::string(), l.writer.str());
        }
        {
            Config().setLevel(Level::DEBUG);
            Logger l = info();
            l << "test";
            EQ(std::string("test"), l.writer.str());
        }
    }

    void filters()
    {
        Config().setFilter("-to,toto,-to*,*");
        {
            Logger l(info("toti"));
            l << "test";
            EQ(std::string(), l.writer.str());
        }
        {
            Logger l(info("toto"));
            l << "test";
            EQ(std::string("test"), l.writer.str());
        }
        {
            Logger l(info("another"));
            l << "test";
            EQ(std::string("test"), l.writer.str());
        }
        EQ(std::string("-to,toto,-to.*,.*"), Config().getFilter());
    }

    void toStringObjects()
    {
        {
            Logger l;
            l << HasToString();
            EQ(std::string("HasToString"), l.writer.str());
        }
        {
            Logger l;
            l << HasExternalToString();
            EQ(std::string("HasExternalToString"), l.writer.str());
        }
        {
            Logger l;
            l << HasBothToString();
            EQ(std::string("HasExternalToString"), l.writer.str());
        }
        {
            Logger l;
            l << InheritToString();
            EQ(std::string("HasExternalToString"), l.writer.str());
        }
        {
            Logger l;
            l << HasDirectOverride();
            EQ(std::string("HasDirectOverride"), l.writer.str());
        }
        info() << LocalTime();
    }

    void elapsed()
    {
        Elapsed el;

        el.start -= chrono::hours(26) + chrono::seconds(65) +
            chrono::milliseconds(250);

        Logger l;
        l << el.tick();
        EQ(std::string("1d2h1m5"), l.writer.str().substr(0, 7));

        l.writer.flush(l.level);
        el.start = chrono::steady_clock::now() - chrono::milliseconds(250);
        l << el.tick();
        EQ(std::string("250ms"), l.writer.str());
    }

    void localTime()
    {
        /* lose ms precision (on purpose) */
        LocalTime::time_point tp = chrono::system_clock::from_time_t(
            chrono::system_clock::to_time_t(chrono::system_clock::now()));
        LocalTime t(tp);
        LocalTime t2(tp + chrono::milliseconds(1));

        debug() << "millisec diff: " << t << " <> " << t2;
        std::string str = to_string(t);
        std::string str2 = to_string(t2);
        EQ(false, str2.empty());
        EQ('1', str2[str2.size() - 1]);
        str2[str2.size() - 1] = '0';
        EQ(str, str2);

        info() << LocalTime();
        std::this_thread::sleep_for(chrono::milliseconds(1));
        info() << LocalTime();
    }

    void category()
    {
        Config().enable(Feature::CATEGORY);
        {
            Logger l(info());
            l << "test";
            EQ(std::string("test"), l.writer.str());
        }

        const std::string category = "my-category";
        {
            Logger l(info(category));
            l << "test";
            EQ(std::string("{my-category} test"), l.writer.str());
        }

        { /* all logs are now padded to maxLen(category) */
            Logger l(info());
            l << "test";
            EQ(std::string(std::string(category.size() + 3, ' ') + "test"),
               l.writer.str());
        }
        /* category larger than the maximum */
        const std::string longCategory(42, 'x');
        { /* displayed properly */
            Logger l(info(longCategory));
            l << "test";
            EQ(std::string("{" + longCategory + "} test"), l.writer.str());
        }
        { /* all logs are now padded to maxCategoryLen */
            Logger l(info());
            l << "test";
            /* max category len = 18 */
            EQ(std::string(std::string(18 + 3, ' ') + "test"), l.writer.str());
        }
    }

    void ios()
    {
        {
            Logger l(info());
            l << std::endl;
            l << std::setfill('x') << std::setw(3) << 2;
            EQ(std::string("\nxx2"), l.writer.str());
        }

        {
            Logger l(info());
            l << std::setprecision(3) << std::scientific << 1.002;
            EQ(std::string("1.002e+00"), l.writer.str());
        }
        // stream is persisted
        EQ(std::string("1.002e+00"), (info() << 1.002).writer.str());
        EQ(std::string("1.002"), (info() << clearflags << 1.002).writer.str());
    }

    void ndebug()
    {
#ifdef NDEBUG
        notice() << "testing ndebug";
        EQ(true, &debug().writer == &NullLogger(Level::DEBUG).writer);
#endif
    }
};

CPPUNIT_TEST_SUITE_REGISTRATION(TestLogger);
