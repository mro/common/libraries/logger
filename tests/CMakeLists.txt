if(TESTS)

link_directories(
  ${CMAKE_BINARY_DIR}/src
  ${CPPUNIT_LIBRARY_DIRS})
include_directories(${CMAKE_BINARY_DIR} ${CMAKE_SOURCE_DIR}/src)
include_directories(SYSTEM
  ${CPPUNIT_INCLUDE_DIRS}
  ${HIDAPI_HEADERS})

file(GLOB_RECURSE test_SRCS RELATIVE "${S}" "test*.cpp" "test*.hpp")

add_executable(test_all ${test_SRCS})
target_link_libraries(test_all ${CPPUNIT_LIBRARIES} logger pthread)
target_compile_definitions(test_all PUBLIC -DDATADIR="${CMAKE_CURRENT_SOURCE_DIR}/data")

add_test(test_all ${CMAKE_CURRENT_BINARY_DIR}/test_all)

add_executable(logger_example Logger_example.cpp Logger_Examples.hpp)
target_link_libraries(logger_example logger pthread)

add_executable(logger_benchmark test_main.cpp Logger_benchmark.cpp)
target_link_libraries(logger_benchmark ${CPPUNIT_LIBRARIES} logger pthread)

if(HAVE_SYSLOG)
  add_executable(logger_syslog Logger_syslog.cpp)
  target_link_libraries(logger_syslog logger pthread)
endif()

endif(TESTS)