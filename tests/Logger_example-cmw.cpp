/*
** Copyright (C) 2022 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2022-10-20T11:34:38
** Author: Sylvain Fargier <sylvain.fargier@cern.ch>
*/

/**
 * @file Logger_example-cmw.cpp
 * @details Logger integration with cmw-log
 */

#include <string>

#include <cmw-log/LogManager.h>
#include <cmw-log/Logger.h>

#include "../src/Logger.hpp"
#include "config.h"

namespace {

cmw::log::Logger &cmwLogger = cmw::log::LoggerFactory::getLogger("cmw.client");

} // namespace

int main()
{
    cmw::log::LogManager::init(SRCDIR "/tests/Logger_example-cmw.cfglog");
    logger::Config()
        .setWriteFunc([](logger::Level level, const std::string &line) {
            switch (level)
            {
            case logger::Level::EMERG: LOG_ERROR_IF(cmwLogger, line); break;
            case logger::Level::ALERT: LOG_ERROR_IF(cmwLogger, line); break;
            case logger::Level::CRIT: LOG_ERROR_IF(cmwLogger, line); break;
            case logger::Level::ERROR: LOG_ERROR_IF(cmwLogger, line); break;
            case logger::Level::WARNING: LOG_WARNING_IF(cmwLogger, line); break;
            case logger::Level::NOTICE: LOG_INFO_IF(cmwLogger, line); break;
            case logger::Level::INFO: LOG_INFO_IF(cmwLogger, line); break;
            case logger::Level::DEBUG: LOG_DEBUG_IF(cmwLogger, line); break;
            default: break;
            }
            return true;
        })
        .disable(logger::Feature::LEVEL)
        .disable(logger::Feature::DATETIME)
        .disable(logger::Feature::TIMEDELTA)
        .enable(logger::Feature::CATEGORY)
        .disable(logger::Feature::THREAD)
        .enable(logger::Feature::NOCOLOR);

    logger::warning("test") << "this is a test";
    return 0;
}
