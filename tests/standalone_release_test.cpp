/*
** Copyright (C) 2023 Sylvain Fargier
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2023-05-16T18:31:50
** Author: Sylvain Fargier <fargier.sylvain@gmail.com>
*/

#include <iostream>

#include <Logger.hpp>

static const std::string logCat{"test"};

class Test
{
public:
    Test() { std::cerr << "Test " << this << std::endl; }
    ~Test()
    {
        std::cerr << "~Test " << this << std::endl;
        logger::warning(logCat) << "~Test";
    }
};

Test t2;
static const Test t3;

/**
 * @brief this is a simple program to ensure that logger is properly released
 * @details the global "t2" instance may be released after LogFacility and
 * ThreadLocalInfo from logger, even tho it tries to log lines
 * it should not crash.
 * Test this with:
 * ```
 * g++ -std=c++11 ../tests/standalone_release_test.cpp -I../src/ -L src -llogger
 * DEBUG=* valgrind ./a.out
 * ```
 */
int main()
{
    logger::warning(logCat) << "starting test";
    Test t;
    logger::warning(logCat) << "exiting test";
}
