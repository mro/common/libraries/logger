/*
** Copyright (C) 2020 Sylvain Fargier
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2022-01-15T17:27:42+01:00
**     Author: Sylvain Fargier <fargie_s> <fargier.sylvain@gmail.com>
**
*/

#include <list>
#include <map>
#include <memory>
#include <set>
#include <string>
#include <tuple>
#include <vector>

#include <cppunit/TestFixture.h>

#include "Logger.hpp"
#include "test_helpers.hpp"

using namespace logger;

class TestLoggerUtilities : public CppUnit::TestFixture
{
protected:
    CPPUNIT_TEST_SUITE(TestLoggerUtilities);
    CPPUNIT_TEST(traits);
    CPPUNIT_TEST(containers);
    CPPUNIT_TEST(largeContainers);
    CPPUNIT_TEST(tuple);
    CPPUNIT_TEST(iterable);
    CPPUNIT_TEST(memory);
    CPPUNIT_TEST_SUITE_END();

    Level m_level;

public:
    void setUp()
    {
        m_level = Config().getLevel();
        Config()
            .reset()
            .setLevel(Level::DEBUG)
            .setFilter("")
            .disable(Feature::LEVEL)
            .disable(Feature::DATETIME)
            .disable(Feature::TIMEDELTA)
            .disable(Feature::CATEGORY)
            .enable(Feature::NOCOLOR);
        std::cerr << std::endl;
    }

    void tearDown() { Config().setLevel(m_level); }

    void traits()
    {
        EQ(false, is_iterable_container<int>::value);
        /* don't consider std::string as an iterable container: not a container */
        EQ(false, is_iterable_container<std::string>::value);
        EQ(true, is_iterable_container<std::vector<int>>::value);
        EQ(true, (is_iterable_container<std::map<int, int>>::value));
        EQ(false, is_iterable_container<std::vector<int>::iterator>::value);

        EQ(true, (has_mapped_type<std::map<int, int>>::value));
        EQ(false, (has_mapped_type<std::vector<int>>::value));
        EQ(false, (has_mapped_type<std::set<int>>::value));
        EQ(false, (has_mapped_type<int>::value));
    }

    void containers()
    {
        { /* list like */
            std::vector<int> vec{1, 2, 3, 4};
            std::set<int> set{1, 2, 3};
            std::list<std::string> list{"test", "another"};
            Logger l(Level::NOTICE);
            l << vec << " " << set << " " << list;
            EQ(std::string("[1,2,3,4] [1,2,3] [test,another]"), l.writer.str());
        }
        { /* map like */
            std::map<int, std::string> test{{1, "str"}, {2, "test"}};
            std::multimap<int, std::string> test2{{1, "str"}, {1, "test"}};
            Logger l(Level::NOTICE);
            l << test << " " << test2;
            EQ(std::string("{1:str,2:test} {1:str,1:test}"), l.writer.str());
        }
    }

    void largeContainers()
    {
        {
            std::list<std::string> list;
            for (std::size_t i = 0; i < 100; ++i)
                list.emplace_back("test");

            Logger l(Level::NOTICE);
            l << list;
            EQ(std::string("[test,test,test,test,test,test,test...]"),
               l.writer.str());
        }

        {
            std::list<std::string> list;
            const std::string str(20, 's');
            for (std::size_t i = 0; i < 100; ++i)
                list.emplace_back(str);

            Logger l(Level::NOTICE);
            l << list;
            EQ(std::string("[ssssssssssssssssssss,ssssssssssssssssssss...]"),
               l.writer.str());
        }

        {
            std::map<std::string, std::string> map;
            for (std::size_t i = 0; i < 100; ++i)
                map[std::to_string(i)] = "test";

            Logger l(Level::NOTICE);
            l << map;
            EQ(std::string("{0:test,1:test,10:test,11:test,12:test...}"),
               l.writer.str());
        }
    }

    void tuple()
    {
        std::tuple<int, int> t{1, 2};
        Logger l(Level::NOTICE);
        l << t;
        EQ(std::string("(1,2)"), l.writer.str());
    }

    void iterable()
    {
        {
            int array[4] = {1, 2, 3, 4};
            Logger l(Level::NOTICE);
            l << iterator(array, 4);
            l << iterator(&array[0], &array[4]);
            EQ(std::string("[1,2,3,4][1,2,3,4]"), l.writer.str());
        }
        {
            std::vector<int> vec{1, 2, 3, 4};
            Logger l(Level::NOTICE);
            l << iterator(vec.begin(), vec.begin() + 2);
            EQ(std::string("[1,2]"), l.writer.str());
        }
        {
            std::list<int> list{1, 2, 3, 4};
            Logger l(Level::NOTICE);
            l << iterator(list.begin(), list.end());
            EQ(std::string("[1,2,3,4]"), l.writer.str());
        }
    }

    void memory()
    {
        std::shared_ptr<std::string> shared;
        std::unique_ptr<std::string> unique;

        {
            Logger l(Level::NOTICE);
            l << shared << " " << unique;
            EQ(std::string("nullptr nullptr"), l.writer.str());
        }

        shared = std::make_shared<std::string>("test");
        unique.reset(new std::string("test"));

        {
            Logger l(Level::NOTICE);
            l << shared << " " << unique;
            EQ(std::string("test test"), l.writer.str());
        }
    }
};

CPPUNIT_TEST_SUITE_REGISTRATION(TestLoggerUtilities);
