
include(Tools)
option(TESTS "Compile and run unit tests" OFF)

include(CheckFunctionExists)
check_function_exists(getopt_long HAVE_GETOPT_LONG)

if(TESTS)

    # CppUnit now requires a recent version of C++
    set_default(CMAKE_CXX_STANDARD 11)

    find_path(CPPUNIT_INCLUDE_DIRS NAMES cppunit/TestFixture.h)
    find_library(CPPUNIT_LIBRARIES NAMES cppunit)
    assert(CPPUNIT_INCLUDE_DIRS AND CPPUNIT_LIBRARIES MESSAGE "Failed to find cppunit")

    add_custom_target(test-verbose
        COMMAND ${CMAKE_CTEST_COMMAND} --verbose
        WORKING_DIRECTORY "${CMAKE_BINARY_DIR}")

    enable_testing()

endif()
