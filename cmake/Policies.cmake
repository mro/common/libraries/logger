# SPDX-License-Identifier: Zlib
# SPDX-FileCopyrightText: 2022 CERN (home.cern)
# SPDX-FileContributor: Author: Sylvain Fargier <sylvain.fargier@cern.ch>

macro(set_policy NAME STATE)
    if(POLICY ${NAME})
        cmake_policy(SET "${NAME}" "${STATE}")
    endif()
endmacro()
