# Maintainer: @CPACK_PACKAGE_CONTACT@
pkgname=@CPACK_PACKAGE_NAME@
pkgver=v@CPACK_PACKAGE_VERSION@
pkgrel=1
pkgdesc=""
arch=('i686' 'x86_64')
url=""
license=('Apache-2.0')
groups=()
depends=()
makedepends=('cmake')
provides=("${pkgname%-git}")
conflicts=("${pkgname%-git}")
replaces=()
backup=()
options=()
install=
source=("@CPACK_PACKAGE_NAME@::git+file://@CPACK_SOURCE_DIR@")
noextract=()
md5sums=('SKIP')

pkgver() {
	cd "$srcdir/${pkgname%-git}"

	# To find latest version tag, use: $(git tag -l 'v*' --sort=-version:refname|head -n 1)
	printf "%s.%s.%s" "v@CPACK_PACKAGE_VERSION@" "$(git rev-list --count HEAD)" "$(git rev-parse --short HEAD)"
}

build() {
	cd "$srcdir/${pkgname%-git}"
	cmake . -DCMAKE_INSTALL_PREFIX=/usr -DCMAKE_BUILD_TYPE=Release
	make
}

package() {
	cd "$srcdir/${pkgname%-git}"
	make DESTDIR="$pkgdir/" install
}
