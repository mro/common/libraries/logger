# SPDX-License-Identifier: Zlib
# SPDX-FileCopyrightText: 2022 CERN (home.cern)
# SPDX-FileContributor: Author: Sylvain Fargier <sylvain.fargier@cern.ch>

# Arguments: SRCDIRS (list of directories to analyse)
separate_arguments(SRCDIRS)
string(TOLOWER "${MODE}" MODE)

include(${CMAKE_CURRENT_LIST_DIR}/Tools.cmake)

set_default(S "${CMAKE_CURRENT_LIST_DIR}/..")
set_default(B "${PROJECT_BINARY_DIR}")

find_program(CLANG_FORMAT_EXE NAMES cquery-clang-format
    HINTS /opt/cquery/bin)
find_program(CLANG_FORMAT_EXE NAMES clang-format)

foreach(F IN LISTS SRCDIRS)
    list(APPEND GLOB_PATTERN "${S}/${F}/*.cpp" "${S}/${F}/*.hpp")
    list(APPEND GLOB_PATTERN "${S}/${F}/*.cc" "${S}/${F}/*.hh" "${S}/${F}/*.h")
    list(APPEND GLOB_PATTERN "${S}/${F}/*.c")
endforeach()
file(GLOB_RECURSE SRCS RELATIVE "${S}" ${GLOB_PATTERN})

if(NOT EXISTS "${S}/.clang-format")
    message(WARNING "No clang-format configuration found, using default")
    configure_file("${CMAKE_CURRENT_LIST_DIR}/ClangFormatScript-template.yml" "${S}/.clang-format" COPYONLY)
endif()

function(gen SRCS)
    foreach(F IN LISTS SRCS)
        message(STATUS "Running Clang Format on \"${F}\"")
        execute_process(
            COMMAND "${CLANG_FORMAT_EXE}" -i "${F}"
            WORKING_DIRECTORY "${S}"
            # ERROR_FILE "${B}/${F}.clang-format-err"
            ERROR_STRIP_TRAILING_WHITESPACE)
    endforeach()
endfunction()

gen("${SRCS}")
