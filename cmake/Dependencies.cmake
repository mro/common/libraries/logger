if(WIN32)
    set(CMAKE_INSTALL_BINDIR "bin" CACHE PATH "")
    set(CMAKE_INSTALL_LIBDIR "bin" CACHE PATH "")
    set(CMAKE_INSTALL_INCLUDEDIR "include" CACHE PATH "")
    set(CMAKE_INSTALL_DATADIR "data" CACHE PATH "")
else(WIN32)
    include(GNUInstallDirs)
endif(WIN32)

include(Depend_cppunit)

include(CheckLibraryExists)
check_library_exists(pthread pthread_setname_np "pthread.h" HAVE_PTHREAD_NAME)

include(CheckIncludeFile)
check_include_file(syslog.h HAVE_SYSLOG)