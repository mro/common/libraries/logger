# SPDX-License-Identifier: Zlib
# SPDX-FileCopyrightText: 2022 CERN (home.cern)
# SPDX-FileContributor: Author: Sylvain Fargier <sylvain.fargier@cern.ch>

include(CMakeParseArguments)

cmake_policy(PUSH)
cmake_policy(SET CMP0057 NEW) # IN_LIST support

macro(use_cxx version)
  if(CMAKE_VERSION VERSION_LESS "3.1")
    if(CMAKE_CXX_COMPILER_ID STREQUAL "GNU")
      set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++${version}")
    endif()
  else()
    set(CMAKE_CXX_STANDARD ${version})
  endif()
endmacro(use_cxx)

macro(use_c version)
  set(CMAKE_C_STANDARD ${version})
endmacro(use_c)

function(assert)
    cmake_parse_arguments(P "FATAL_ERROR" "MESSAGE" "" ${ARGN})
    if(${P_UNPARSED_ARGUMENTS})
        # Can't use NOT since there may be several tests, so doing it that way
        return()
    elseif(NOT P_MESSAGE)
        set(P_MESSAGE "Assertion failed:")
        foreach(PART ${P_UNPARSED_ARGUMENTS})
            set(P_MESSAGE "${P_MESSAGE} ${PART}")
        endforeach()
    endif()
    if(P_FATAL_ERROR)
      message(FATAL_ERROR "${P_MESSAGE}")
    else()
      message(SEND_ERROR "${P_MESSAGE}")
    endif()
endfunction()

function(set_default variable value)
  cmake_parse_arguments(P "ENV" "" "" ${ARGN})

  if("PARENT_SCOPE" IN_LIST P_UNPARSED_ARGUMENTS)
    # This should be fixed in 3.27 using a macro() and block()
    message(SEND_ERROR "set_default doesn't support PARENT_SCOPE (not a macro)")
  elseif(NOT "CACHE" IN_LIST P_UNPARSED_ARGUMENTS)
    list(APPEND P_UNPARSED_ARGUMENTS "PARENT_SCOPE")
  endif()

  if (P_ENV AND DEFINED ENV{${variable}})
    set(${variable} $ENV{${variable}} ${P_UNPARSED_ARGUMENTS})
  elseif(NOT DEFINED ${variable})
    set(${variable} ${value} ${P_UNPARSED_ARGUMENTS})
  else()
    set(${variable} ${${variable}} ${P_UNPARSED_ARGUMENTS})
  endif()
endfunction()

cmake_policy(POP)
