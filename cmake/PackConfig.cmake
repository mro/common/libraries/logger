# SPDX-License-Identifier: Zlib
# SPDX-FileCopyrightText: 2022 CERN (home.cern)
# SPDX-FileContributor: Author: Sylvain Fargier <sylvain.fargier@cern.ch>

if (EXISTS "${CMAKE_CURRENT_LIST_DIR}/../.git/shallow")
  message(SEND_ERROR "Not generating package on shallow clone, use \"git fetch --unshallow\"")
endif ()
