# 👀 Logger

The logger that doesn't get in the middle.

Simple, configuration-free, yet feature-complete and performant logger.

## Features

- C++11
- no dependencies
- Fast multi-threaded and Lock-free implementation
- Standard 7 syslog levels
- Application level log categories and easy filtering
- Fancy unicode and colored output in TTY
- Easy configuration (using environment)
- Smart STL container support (see below)

![diagram](./data/logger.png)

[![asciicast](https://asciinema.org/a/460851.svg)](https://asciinema.org/a/460851)

## Usage

In the code:
```cpp
#include <logger/Logger.hpp>

using namespace logger; // or prefix everything with logger::

int main() {
  debug() << "debug message";

  notice("cat") << "categorized message";
}
```

At run-time:
```bash
# Log everything and enable DEBUG level
DEBUG=* ./tests/logger_example
# Equivalent to:
LOG_LEVEL=debug ./tests/logger_example

# Disable app:part category but enable all other app:*
DEBUG=-app:part,app:* ./tests/logger_example
```
**Note:** see _Configuration_ above for further details

## Logging Custom Objects

To implement custom types, one must either implement (by order of priority):
```cpp
class MyObject { /* ... */ };

/* a Logger stream operator */
const Logger &operator <<(const Logger &logger, const MyObject &a);
```

```cpp
class MyObject { /* ... */ };

/* a to_string conversion function */
std::string to_string(const MyObject &a);
```

```cpp
class MyObject
{
public:
  /* a toString() method */
  std::string toString() const;
};
```

See [Logger_Examples.hpp](./tests/Logger_Examples.hpp) for more details

## Configuration

At run-time, the following variables can be used to configure the logger:
- **LOG_LEVEL** using either _ALERT, CRIT, DEBUG, EMERG, ERROR, INFO, NOTICE, WARNING_
- **LOG_CATEGORY** using a list of categories
- **LOG_FEATURE** using a list of features
- **DEBUG** same as _LOG_CATEGORY_ automatically enabling _DEBUG_ level.

**Note:** default configuration is equivalent to `LOG_LEVEL=notice` `LOG_CATEGORY=*`.

**Note:** levels are not case sensitive.

**Note:** category list is comma separated, it supports globing using **\*** and
categories can be disabled using **-** prefix, first match is taken in account
(from left to right).

**Note:** feature list is comma separated, features can be disabled using **-** prefix.

Example:
```bash
# Log everything and enable DEBUG level
DEBUG=* ./tests/logger_example
# Equivalent to:
LOG_LEVEL=debug ./tests/logger_example

# Disable app:part category but enable all other app:*
DEBUG=-app:part,app:* ./tests/logger_example
```

Programatically, a `Config` object is available to modify the configuration:
```cpp
#include <logger/Logger.hpp>

using namespace logger;

int main() {
  Config()
  .reset() // reset to defaults from env
  .setLevel(logger::Level::DEBUG) // set level to debug
  .setFilter("app:*") // set category filter (empty string to clear)
  .enable(Feature::LEVEL) // show level on each line (icon on TTY, text otherwise)
  .enable(Feature::DATETIME) // show timestamp on each line (millisecond precision)
  .enable(Feature::TIMEDELTA) // show time-delta between each line (per-thread)
  .enable(Feature::CATEGORY) // show category (in curly-braces)
  .disable(Feature::NOCOLOR); // disable colors (automatically disable on non-TTY output)

  debug() << "test";
}
```

**Note:** all Config methods (like complete logger API) are thread-safe and
can be invoked from any thread.

### Third-party integration

The logger writing function may be replaced by a custom hook, to easily
integrate third-party logging mechanism:
```cpp
#include <logger/Logger.hpp>

using namespace logger;

int main() {
  std::ofstream ofs("/tmp/log.txt");
  Config().setWriteFunc([&ofs](Level level, const std::string &line) => {
    ofs << line;
    return true;
  });

  notice() << "test";
  Config().setWriteFunc(nullptr);
  ofs.close();
}
```

**Note:** a syslog integration example is available [here](./tests/Logger_syslog.cpp)

An example integration for the [CERN CMW Logger](https://wikis.cern.ch/display/MW/Log+and+Tracing) is described [here](./CMW_INTEGRATION.md).

## Utilities

Some utilities are available to make logging easier.

## Time measurement

The `LocalTime` and `Elapsed` objects are available to measure and display
time_points, example:
```cpp
#include <chrono>
#include <logger/Logger.hpp>

using namespace logger;

int main()
{
  // current time
  notice() << LocalTime();
  // A specific time_point
  notice() << LocalTime(std::chrono::system_clock::now());

  // Measure elapsed time
  Elapsed elapsed;
  // tick counter and display it
  notice() << "elapsed time: " << elapsed.tick();
  // dump any duration (with millisecond precision)
  notice() << "duration: " << std::chrono::milliseconds(300);

  return 0;
}
```
**Output:**
```bash
2022-01-16 13:15:55.043 (    0ms) 👀 2022-01-16 13:15:55.043
2022-01-16 13:15:55.043 (    0ms) 👀 2022-01-16 13:15:55.043
2022-01-16 13:15:55.043 (    0ms) 👀 elapsed time: 0ms
2022-01-16 13:15:55.043 (    0ms) 👀 duration: 300ms
```

## Containers

Any iterable container (as in [Container](https://en.cppreference.com/w/cpp/named_req/Container),
more specifically range-iterable) can be dumped in the Logger.

**Note:** on non-debug stream a limitation of 20 elements and/or 30 characters
is applied to containers, adding an elipsis (`...`) at the end of container when
one of this limit is reached (to prevent large-containers dump side-effects) ;
to dump complete containers, do use `Level::DEBUG`.

Examples:
```cpp
#include <logger/Logger.hpp>
#include <vector>
#include <set>
#include <list>
#include <map>

int main()
{
  std::vector<int> vec{1, 2, 3, 4};
  std::set<int> set{1, 2, 3};
  std::list<std::string> list{"test", "another"};

  logger::Logger l(logger::Level::NOTICE);
  l << vec << " " << set << " " << list;

  std::multimap<int, std::string> multimap{{1, "str"}, {1, "test"}};
  l << " " << multimap;

  for (std::size_t i = 0; i < 100; ++i)
    list.emplace_back("test");
  l << " " << list;

  return 0;
}
```
**Output:** `2022-01-16 12:58:24.247 (    0ms) 👀 [1,2,3,4] [1,2,3] [test,another] {1:str,1:test} [test,another,test,test,test,test...]`

**Note:** Operators for `std::pair` and `std::tuple` are also provided.

Support for plain arrays and container parts is provided through a dedicated `logger::iterator`, see:
```cpp
#include <logger/Logger.hpp>
#include <vector>

int main()
{
  int array[]={1, 2, 3, 4};
  std::vector<int> vec{1, 2, 3, 4};

  logger::Logger l(logger::Level::NOTICE);
  l << logger::iterator(array, 4);
  l << " " << logger::iterator(&array[1], &array[3]);

  l << " " << logger::iterator(vec.begin(), vec.end());
  l << " " << logger::iterator(vec.begin() + 1, vec.begin() + 3);

  return 0;
}
```
**Output:** `2022-01-16 13:13:06.497 (    0ms) 👀 [1,2,3,4] [2,3] [1,2,3,4] [2,3]`
