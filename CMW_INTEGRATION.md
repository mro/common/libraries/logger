# CMW Logger Integration

The [CERN CMW logger](https://wikis.cern.ch/display/MW/Log+and+Tracing) may easily
be integreated with this library.

Here's an example integration: [Logger_example-cmw.cpp](./tests/Logger_example-cmw.cpp)

To compile it:
```bash
mkdir build && cd build
cmake3 .. && make -j4

g++ -std=c++11 ../tests/Logger_example-cmw.cpp -o Logger_example-cmw \
    -Lsrc -I../src -I. -llogger \
    -I/acc/local/L867/cmw/cmw-log/4.0.1/include \
    -L/acc/local/L867/cmw/cmw-log/4.0.1/lib \
    -lcmw-log -lcmw-util -lboost_1_69_0_thread -lboost_1_69_0_filesystem -lpthread

LD_LIBRARY_PATH=src ./Logger_example-cmw
```